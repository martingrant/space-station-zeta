#######################################################
####### SPACE STATION ZETA ##############################
#######################################################
####### CREATED BY MARTIN GRANT #########################
#######################################################

Control "Helix" through the takeover of "Space Station Zeta"

Created for university coursework. If you would like to use anything, please contact me.

Martin Grant
www.martingrant.net
me@martingrant.net
martin-grant@hotmail.com
@mrtgnet


Credits:

"Spazzmatica Polka" and "The House of Leaves" by Kevin Macleod - incompetech.com

Various SFX by opengameart.org and Yo Yo Games

Artwork by Martin Grant, Callum Lyons, roencia.com, opengameart.org, widgetworx.com, renderat.com and Yo Yo Games

Developed using Yo Yo Game's "GameMaker"

